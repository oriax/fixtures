<?php

namespace Oriax\Fixtures;

use Oriax\Fixtures\Exceptions\FixtureNotFoundException;
use Oriax\Fixtures\Exceptions\FixtureProviderNotRegistered;

class Fixtures implements FixturesInterface
{

    /**
     * @var FixturesInterface
     */
    protected static $instance;

    /**
     * @var \SplDoublyLinkedList
     */
    private $fixtureProviders;

    /**
     * @var \SplDoublyLinkedList
     */
    private $fixtures;

    /**
     * @return FixturesInterface
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Fixtures();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->fixtureProviders = new \SplDoublyLinkedList();
        $this->fixtures = new \SplDoublyLinkedList();
    }

    /**
     * @param string $name
     * @param callable $callableObject
     */
    public function add($name, $callableObject)
    {
        $this->fixtures->add($name, $callableObject);
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get($name)
    {
        $this->loadFixtureProvider($name);
        if (!$this->fixtures->offsetExists($name)) {
            throw new FixtureNotFoundException('Fixture not found');
        }

        $callable = $this->fixtures->offsetGet($name);
        return call_user_func($callable, $this);
    }

    /**
     * @param string $name
     *
     * @return void
     */
    protected function loadFixtureProvider($name)
    {
        if (!$this->fixtureProviders->offsetExists($name)) {
            throw new FixtureProviderNotRegistered('Fixture provider is not registered!');
        }

            /** @var FixtureProviderInterface $provider */
        $provider = $this->fixtureProviders->offsetGet($name);
        $provider->registerFixtures($this);
    }

    /**
     * @param FixtureProviderInterface $fixtureProvider
     *
     * @return void
     */
    public function registerFixtureProvider(FixtureProviderInterface $fixtureProvider)
    {
        $providesFixtures = $fixtureProvider->providesFixtures();

        foreach ($providesFixtures as $fixtureName) {
            $this->fixtureProviders->add($fixtureName, $fixtureProvider);
        }
    }

}
