<?php

namespace Oriax\Fixtures;

interface FixtureProviderInterface
{

    /**
     * @param FixturesInterface $fixtures
     * @return mixed
     */
    public function registerFixtures(FixturesInterface $fixtures);

    /**
     * @return \ArrayObject
     */
    public function providesFixtures();

}
