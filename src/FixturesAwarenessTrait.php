<?php

namespace Oriax\Fixtures;

trait FixturesAwarenessTrait
{

    /**
     * @return Fixtures
     */
    protected function getFixtures()
    {
        return Fixtures::getInstance();
    }

}
