<?php

namespace Oriax\Fixtures\Exceptions;

class FixtureNotFoundException extends \RuntimeException
{
}
