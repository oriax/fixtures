<?php

namespace Oriax\Fixtures\Exceptions;

class FixtureProviderNotRegistered extends \RuntimeException
{
}
